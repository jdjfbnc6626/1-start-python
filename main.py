from random import randint
from lib.UserInput import get_numeric_input
from lib.CharacterMap import convert_to_char

def get_random_number_between_vals(val1, val2):
    return randint(val1, val2)

password_length = get_numeric_input("Please enter length of password (0-100) ")

# bottom_ascii =
# top_ascii = 
# instructor - Brian - made a function using a case-lookin-statement to implement this

password_list = [None] * password_length

for i, item in enumerate(password_list):
    # Change random number vals to bigger spread - looks like this opened ASCII space to all kindsa wierd characters
    # Yes - https://www.ibm.com/docs/en/sdse/6.3.1?topic=configuration-ascii-characters-from-33-126
    random_number = get_random_number_between_vals(33, 126)
    password_list[i] = convert_to_char(random_number)

password = "".join(password_list)
print(password)
